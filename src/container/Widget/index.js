import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  getWeatherDetails,
} from './actions';

const WidgetStyles = {
  container: {
    height: "300px",
    width: "60%",
    margin:"auto",
    border: "1px solid #ccc",
    backgroundColor: "#f3f3f3",
    marginTop: "100px",
    display: "flex",
    fontFamily: "sans-serif",
    color: "darkslategrey"
  },
  form: {
    height: "100%",
    width: "50%",
    borderRight: "1px solid rgb(204, 204, 204)"
  },
  leftContent:{
    margin:"auto",
    height: "80%",
    width:"80%",
    marginTop: "20px",
    display: "flex",
    flexDirection: "column"
  },
  rightContent:{
    margin:"auto",
    height: "80%",
    width:"80%",
    marginTop: "20px",
    display: "flex",
    flexDirection: "column",
    backgroundColor: "white",
    borderRadius: "5px",
    boxShadow: "0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)"
  },
  info: {
    height: "100%",
    width: "50%",
    margin:"auto",
  },
  row: {
    display: "inline-grid",
    margin: "15px",
  },
  title:{
    height: "30px",
    padding: "5px",
    marginTop: "10px",
  },
  lowerLabel: {
    marginTop: "10px",
  },
  innerLabel: {
    marginLeft: "50px"
  },
  widgetTitle:{
    textAlign: "center",
    fontSize: "large",
    marginTop: "20px",
    fontWeight: "bold"
  },
  weatherInfo: {
    display: "flex",
    marginTop: "35px"
  },
  infoRowLeft: {
    margin:"10px 0 0 30px"
  },
  infoRowRight: {
    margin:"10px 0 0 10px",
    display: "flex",
    flexDirection: "column"
  },
  temp:{
    fontSize: "45px",
    fontWeight: "bold",
    color: "black"
  }
}

class Widget extends Component {
  static propTypes = {
    message: PropTypes.string,
    getWeatherDetails: PropTypes.func,
    weatherDetails: PropTypes.object,
  };

  static defaultProps = {
    getWeatherDetails: f => f,
    weatherDetails: {},
  }

  /* Setting default state */
  
  state = {
      title: "Title of Widget",
      latitude: "",
      longitude: "",
      weatherDetails: {},
      celcius: true,
      showWind: true,
      isData: false,
    };

  componentWillReceiveProps(nextProps) {
    const { weatherDetails } = nextProps;
    if ((weatherDetails) !== this.props.weatherDetails) {
      /* Converting temperature Kelvin to Celcius */
      let temp = parseFloat(weatherDetails.temperature);
      temp = (temp - 273.15).toFixed(2);
      let windspeed = parseFloat(weatherDetails.windspeed);
       /* Converting wind speed m/sec to km/hr */
      windspeed = (windspeed*3.6).toFixed(2);
       /* Setting the state with new values */
      this.setState({ weatherDetails: { ...weatherDetails, temperature: temp, windspeed }, isData:weatherDetails.isData });
    }
  }

  componentDidMount() {
    /* fetching latitude and longitude based on current position */
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(this.showPosition, this.showError,{timeout:10000});
      this.showPosition
    }
  }

  handleTitleChange = (e) => {
    /* Checking if the title has length or else adding common label */
    const title = e.target.value.length? e.target.value : "Title of widget";
    this.setState({ title });
  }

  handleTempChange = (e) => {
    /* Changing temperature value between metrics */
    const { weatherDetails , isData, celcius} = this.state;
    if( isData ){
    let temperature = parseFloat(weatherDetails.temperature);
    let celcius = true;
    if (e.target.value == "C") {
      temperature = ((temperature - 32.00) * (5 / 9)).toFixed(2);
    } else {
      temperature = ((temperature * 9 / 5) + 32).toFixed(2);
      celcius = false;
    }
    this.setState({ weatherDetails: { ...weatherDetails, temperature }, celcius });
  } else 
   this.setState({celcius: !celcius});
  }

  handleWindChange = (e) => {
    /* Showing or hiding  wind speed */
    let showWind = false;
    if (e.target.value == "On")
      showWind = true;
    else showWind = false;
    this.setState({ showWind });
  }

  showPosition = (position) => {
    /* Getting position coordinates aand then fetching weather details */
    const { getWeatherDetails } = this.props;
    const latitude = position.coords.latitude;
    const longitude = position.coords.longitude;
    this.setState({
      latitude,
      longitude,
    });
    if(latitude && longitude )
     getWeatherDetails({ latitude, longitude });
  }

  showError = (err) => {
    console.log("unexpected error from navigator.geolocation-----> ", err);
  }

  render() {
    const { title, weatherDetails, celcius, showWind, isData } = this.state;
    const wind = showWind ? (<span>Wind {weatherDetails.windspeed} km/hr</span>) : null;
    const data = isData ?
          (<div style={WidgetStyles.infoRowRight}>
              <span>{weatherDetails.city}</span>
              <span style={WidgetStyles.temp}>{weatherDetails.temperature}º</span>
              {wind}
            </div>): "No Data Available";
    return (
      <div style={WidgetStyles.container} >
        <div style={WidgetStyles.form}>
          <div style={WidgetStyles.leftContent}>
          <div style={WidgetStyles.row}>
            <label>
              Title
            </label>  
            <input
                name='title'
                onChange={this.handleTitleChange}
                style={WidgetStyles.title}
                placeholder="Title of widget"
            />
          </div>
          <div style={WidgetStyles.row}>
            <label>
              Temperature
            </label>
            <span style={WidgetStyles.lowerLabel}>
            <label>
              <input type="radio" value="C" name="temperature" checked={celcius} onChange={this.handleTempChange} />
              ℃
            </label>
            <label style={WidgetStyles.innerLabel}>
              <input type="radio" value="F" name="temperature" checked={!celcius} onChange={this.handleTempChange} />
              ℉
            </label>
            </span>
          </div>
          <div style={WidgetStyles.row}>
            <label>
              Wind
            </label>
            <span style={WidgetStyles.lowerLabel}>
            <label>
              <input type="radio" value="On" name="wind" checked={showWind} onChange={this.handleWindChange} />
              On
            </label>
            <label style={WidgetStyles.innerLabel}>
              <input type="radio" value="Off" name="wind" checked={!showWind} onChange={this.handleWindChange} />
              Off
            </label>
            </span>
          </div>
          </div>
        </div>

        <div style={WidgetStyles.info}>
        <div style={WidgetStyles.rightContent}>
            <div style={WidgetStyles.widgetTitle}>
            <span>{title}</span>
            </div>
            <div style={WidgetStyles.weatherInfo}>
            <div style={WidgetStyles.infoRowLeft}>
            <img src="../src/container/Images/cl.jpg"
                  alt="weather"
                  width="130px"
                  height="110px"
                />
            </div>
            {data}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({ widgetReducer }) => {
  const {
    weatherDetails,
  } = widgetReducer;
  return {
    weatherDetails,
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getWeatherDetails,
    },
    dispatch,
  );

export default connect(mapStateToProps, mapDispatchToProps)(Widget);




