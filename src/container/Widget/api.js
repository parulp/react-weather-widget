import axios from 'axios';

  export const getWeatherData = params => () =>
  axios.get(`https://api.openweathermap.org/data/2.5/weather?lat=${params.latitude}&lon=${params.longitude}&appid=0cc1938997ad472774921abcfdfc8dd5`);