/**
 * Action creators
 * These are just objects that indicates what to do but doesn"t actually perform anything
 */

import {
  getWeatherData
} from './api';

export const WEATHER_DETAILS = 'WEATHER_DETAILS';

export const getWeatherDetails = params => ({
  type: WEATHER_DETAILS,
  payload: getWeatherData(params),
});