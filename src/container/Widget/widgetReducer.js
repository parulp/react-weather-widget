import typeToReducer from 'type-to-reducer';
import {
  WEATHER_DETAILS
} from "./actions";

const initialState = {
  cityDetails: {},
  weatherDetails: {}
};

const widgetReducer = typeToReducer({
  [WEATHER_DETAILS]: {
    PENDING: state => ({
      ...state,
    }),
    FULFILLED: (state, action) => {
      /* handling if data is not returned */
      let temperature = "No Data";
      let windspeed = "No Data";
      let city = "No Data";
      let isData = false;
      if (action.payload.data.main && action.payload.data.wind && action.payload.data.name)
      {
        temperature = action.payload.data.main.temp;
        windspeed = action.payload.data.wind.speed;
        city = action.payload.data.name;
        isData = true;
      }
    return ({
      ...state,
      weatherDetails:{
        temperature, 
        windspeed, 
        city,
        isData },
    })
  },
    REJECTED: state => ({
        ...state,
    }),
  },
}, initialState);

export default widgetReducer;
